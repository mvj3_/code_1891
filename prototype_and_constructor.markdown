prototype是function的prototype，constructor指向function本身。

prototype通过function属性 `__proto__ ` 来指定继承关系，它是属性和function的集合。

在Chrome里调试代码如下
```javascript
h = function() { return 1; }
h2  = new h();
h2.constructor == h // true

h.__proto__ // function Empty() {}
h.__proto__ === Object.prototype // false
h.__proto__.__proto__ == Object.prototype // true

f1 = function() { this.getName = function() { return 'mvj3'; } }
f11 = new f1()
f2 = function() {}
f2.prototype = f11
f22 = new f2()
f22.getName() // 'mvj3'
```

想深入熟悉JavaScript里的核心概念，推荐看jQuery之父写的教程 http://ejohn.org/apps/learn/